# Repository for Conquaire quality checks

Implementation of the Quality Checks from the Conquaire Project. Checks cover CSV and XML filetype. In addition minimal FAIR checks are executed which check for existence of README, AUTHORS and LICENSE file.

# folder structure
* _data/_: Some test data for csv and xml are stored in the folders _csv_data_ and _xml_data_ for offline testing purpose.
* _docs/_: Folder contains visualisation of the quality checks pipeline and a description taken from the Conquaire book which contains an overall description about how the Conquaire quality checks work.
* _results/_: Some test output from the quality checks for the given test data inside the _data/_ folder.
* _src/_: All created source code and requirements.txt can be found here. It is written in Python3.

# usage of quality checks
For offline testing you can execute the _offline_exec_main.sh_ bash script to trigger the pipeline manually. Type the following in your terminal after directing to the repository root folder:

```
./offline_exec_main.sh
```

There is also a _.gitlab-ci.yml_ which triggers the CI runner after every commit and executes the quality checks pipeline automatically on the Conquaire server.
To execute it you just have to clone the repository into your namespace and make a commit and push to your repository. You will be informed about the quality check results via mail which contains an url your results.

# description of source code inside _src/_
* _csv_check/_: Implementation of a csv check. Additional _.ini_ file can be provided next to your data to execute further semantical data checks.
* _fair_check/_: Implementation of minimal __FAIR__ checks. It checks for existence of _README_, _AUTHORS_ and _LICENSE_ file inside your repository.
* _html_check/_: Implementation of overview creation. Script creates a _.HTML_ file which can be accessed via browser and shows quality check results.
* _xml_check/_: Implementation of a xml check. Additional _.dtd_ file can be provided next to your data to execute further semantical data checks.
* _mail/_: Implementation of a script which sends feedback mail to user after a commit and execution of CI pipeline containing an url to the quality check results.
* _main.py_: This script executes the whole quality checks pipeline. New filetype specific tests can be added here.

# usage of [_.gitlab-ci.yml_](https://docs.gitlab.com/ee/ci/quick_start/) file:
To set up the _.yml_ for your repository correctly you just have to adjust the data path behind the _-d_ parameter to fit with your folder structure. Type inside the quotation marks the local repository path of your scientific data. In this example a folder named "data" contains all scientific data which should be checked by quality checks.
If you have any issues with your runner, there is an additional _.gitlab-ci.yml_debug_ which echos the [GitLab CI variables](https://docs.gitlab.com/ee/ci/variables/) to the console output.

```
quality-check:

  # Use smallest docker python image.
  image: python:3.6-alpine

  before_script:
    # Create temporary mail configuration files.
    - mkdir /etc/ssmtp
    - echo "root=${GITLAB_USER_EMAIL}" > /etc/ssmtp/ssmtp.conf
    - echo "mailhub=conquaire.uni-bielefeld.de" >> /etc/ssmtp/ssmtp.conf
    - echo "hostname=gitlab-runner.conquaire.uni-bielefeld.de" >> /etc/ssmtp/ssmtp.conf
    # Install python lxml and ssmtp package for sending feedback mail in the docker container.
    - apk add py3-lxml ssmtp

  script: 
    - /usr/bin/python3 /opt/conquaire/quality_checks/src/main.py 
    -f /var/www/html/feedback/ 
    -l "https://conquaire.uni-bielefeld.de/feedback/"
    -r "$(pwd)" 
    -d "data" 
    -gn "${GITLAB_USER_NAME}" 
    -ge "${GITLAB_USER_EMAIL}" 
    -gu "${CI_PROJECT_URL}" 
    -gp "${CI_PROJECT_PATH}" 
    -gs "${CI_COMMIT_SHA}"
  
  # Choose docker CI runner on gitlab server.
  tags:
    - dockerexec  
```