\chapter{Conquaire Quality Check}

\section{Overview} \label{overview}
A part of the Conquaire project was the development of automated data quality tests for researchers.
The quality checks are integrated into the \emph{Gitlab} platform from the University of Bielefeld.
The checks are written in \emph{Python 3.6} and use the \emph{lxml} package \footnote{\url{https://lxml.de/}} for parsing XML files as the only external requirement.
The pipeline of the quality check is shown in Figure~\ref{Fig-1: qc_pipeline} below. All steps are described in sections below.
\begin{figure}[!h]
    \centering
    \includegraphics[width=9cm,height=10.5cm,keepaspectratio]{cb02-Technical/images/qc_pipeline.pdf}
    \caption{Workflow of Conquaire Quality Check.}
    \label{Fig-1: qc_pipeline}
    \protect
\end{figure}

By adding a preconfigured YAML file (in this case: \emph{.gitlab-ci.yml}) to a repository on the Gitlab instance the checks are automatically executed via a continuous integration runner on the Gitlab server. \\
The runner creates a docker container. As the docker image we use the python:3.6-alpine image because it is lightweight and only contains an installed version of Python 3.6. In addition to that, we install the \emph{lxml} package and a \emph{SMTP}\footnote{\url{https://wiki.debian.org/sSMTP}} instance to notify the user about the results from a check.
The user is informed via email. The mail contains information about the repository and a URL to the detailed feedback which is provided in a web browser. The mail also shows the user the overall test result which is displayed as a badge icon. The same icon is displayed in PUB if the user decides to create a data publication.

\section{Example of pre-configured YAML file}
The pre-configured file has to be stored in the root folder of the repository. For each commit to the repository, it is automatically executed by the CI runner and performs the Conquaire quality checks for the given repository. The user only has to change the value of the \texttt{-d} parameter as it represents the local path to the data inside the repository. In the given example, a folder named \emph{data} inside the repository contains the files which should be tested.
\begin{lstlisting}
quality-check:
  # Use smallest docker python image.
  image: python:3.6-alpine
  before_script:
    # Create temporary mail configuration files.
    - mkdir /etc/ssmtp
    - echo "root=${GITLAB_USER_EMAIL}" > /etc/ssmtp/ssmtp.conf
    - echo "mailhub=conquaire.uni-bielefeld.de" >>
        /etc/ssmtp/ssmtp.conf
    - echo "hostname=gitlab-runner.conquaire.uni-bielefeld.de"
        >> /etc/ssmtp/ssmtp.conf
    # Install lxml and ssmtp package for sending feedback mail.
    - apk add py3-lxml ssmtp
  script:
    # Execute quality checking pipeline.
    - /usr/bin/python3 /opt/conquaire/quality_checks/src/main.py
        -f /var/www/html/feedback/ 
        -l "https://conquaire.uni-bielefeld.de/feedback/" 
        -r "$(pwd)" 
        -d "data" 
        -gn "${GITLAB_USER_NAME}" 
        -ge "${GITLAB_USER_EMAIL}" 
        -gu "${CI_PROJECT_URL}" 
        -gp "${CI_PROJECT_PATH}" 
        -gs "${CI_COMMIT_SHA}"
  # Choose docker CI runner on gitlab server.
  tags:
    - dockerexec  
\end{lstlisting}

The whole pipeline is executed in a docker container and makes use of continuous integration variables provided by Gitlab. They are automatically filled with the information from the users Gitlab profile.

\section{Quality checks}
The Conquaire Quality Check pipeline involves a variety of tests that are automatically performed on the Git repository. Each time a commit occurs, the Gitlab CI runner calls our pipeline, and several different scripts are executed to guarantee that the provided data is in the best possible state. The three main checks that are implemented, are the FAIR check, the CSV check, and the XML check. The pipeline is designed to be very modular and flexible to make it as easy as possible to extend it with further checks, i.e., for additional file types.

\medskip
Every check begins with searching the repository and generating a list of every file with the specific type using the bash \texttt{find} command. For each file that was found, the corresponding test script is called to perform the actual checks and generate a log file with errors and warnings that were observed. The details of the three specific checks are described below. In the end, an overall feedback file is created, showing the results of the checks with links to the log files, making it possible to look into the data and correct it if necessary. The contributor is informed about the results of the pipeline via email.

\subsection{FAIR check}
In our adaptive implementation of the FAIR metrics\footnote{\url{http://fairmetrics.org/}}, we check if the three necessary files exist in the repository: the AUTHORS, LICENSE, and README files.\\
The files have to be placed in the root directory of the repository to fulfill the test condition. The files have to have either no extension, plain text (\texttt{.txt}) or markdown (\texttt{.md}). \\
We suggest to save the files as markdown files.
The markdown file type is used as a standard in Gitlab and many other websites because it has an easy to learn syntax and can be displayed in a web browser.

\medskip
The AUTHORS file should contain a list of all the contributors and their emails for the possibility to contact them. The LICENSE file should describe how the data can be further used and distributed by other researchers, either by declaring one of the common licenses or providing their own. The README file should contain every other information that is related to the data and necessary or helpful to understand the research that was done, e.g., a description of the data or the experiment to obtain it.

\newpage
\subsection{CSV check}
In the CSV file format (\texttt{.csv}), data is organized as a table with comma separated values. The first step in the CSV check is to test whether the file can be opened and the table is well-formed, i.e., it has a header and a consistent number of rows and columns.\\
The researcher can provide an additional format declaration file (\texttt{.ini}) with his own specifications of the data, e.g., the type of the column and the expected range of the values. The quality check reports a warning if a required entry is missing or a value is out of range or has a wrong type, e.g., a non-numeric value in a numeric column.\\
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.67\textwidth]{cb02-Technical/images/csv.png}
\end{center}
\caption{Example result of the CSV check.}
\label{fig:csv}
\end{figure}\\
The log file lists all the errors and warnings that were found, and the row and column in which they occurred. In addition to that, the corresponding cell is marked in the table, allowing to conveniently find problematic entries, as seen in the example in Figure~\ref{fig:csv}.

\subsection{XML check}
In the XML file format (\texttt{.xml}), data is organized as a tree structure in markup language. The first step in the XML check is to test whether the file can be opened and the document is well-formed, i.e., the syntax of the markup language tags is correct.\\
The researcher can provide an additional doctype definition file (\texttt{.dtd}) with his own specifications of the data, e.g., the required attributes and some restrictions to the values. The quality check reports an error if there is a mismatch of opening and closing tags, and a warning if the specifications are not fulfilled, e.g., a value is missing.\\
\newpage
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.67\textwidth]{cb02-Technical/images/xml.png}
\end{center}
\caption{Example result of the XML check.}
\label{fig:xml}
\end{figure}

The log file lists all the errors and warnings that were found, as well as the line in which they occurred. In addition to that, the corresponding line is marked in the document, allowing to conveniently find problematic entries, as seen in the example in Figure~\ref{fig:xml}.

\section{Results and Conclusion}
After successful execution, the Conquaire quality check pipeline produces an overall result HTML file which contains visual feedback of all individual tests and links to the resulting LOG and optional HTML files. An example is provided in Figure~\ref{fig:result}. \\
The feedback shows one of three different colors and badges. A green badge symbolises a successful test result, i.e., the data is valid. A yellow badge indicates well-formed data and the LOG files can contain some warnings.
A red badge indicates not well-formed data or missing FAIR files. The user should check the LOG files and fix the errors before submitting a data publication.
The URL of the overall result is provided to the user via email. This mail is sent automatically after every execution.\\
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.67\textwidth]{cb02-Technical/images/result.png}
\end{center}
\caption{Example result of the overall result.html.}
\label{fig:result}
\end{figure}
In addition to that, an overall badge icon is created. This badge is equivalent to the badge of the worst individual check result.
This badge is displayed in PUB\footnote{\url{https://pub.uni-bielefeld.de/}} if the user decides to create a data publication from the repository. The badge is equal to one of the three different symbols shown in Figure~\ref{fig:result}. \\

In conclusion, the Conquaire quality checks are designed to help the researchers provide better research data and make their publications as good as possible.
Fulfilling the FAIR metrics is highly important for the reproducibility of the data as they are necessary to provide other researchers the information and legal basis to use the data for their consecutive works.
The file type specific checks help finding and fixing errors before releasing the data to the public. This is fundamental for reproducibility as only valid data can be used to recreate the experiment results.
