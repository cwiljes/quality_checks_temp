#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is the quality check for CSV files.
#

import os, csv, re
import configparser
from math import inf

#
# FORMAT SPECIFICATION OPTIONS:
#
# + FOR ALL COLUMNS:
# + + type = integer|float|bool|date|datetime|string
# + + required = True|False|Yes|No
#
# + FOR NUMERIC COLUMNS:
# + + minval = <integer>|<float>
# + + maxval = <integer>|<float>
#
# + FOR STRING COLUMNS:
# + + minlen = <integer>
# + + maxlen = <integer>
# + + pattern = <regular expression>
#

# Global variables used to store the results.
LOG = ""
HTM = ""
FMT = {}

# Common date formats used for parsing.
date_fmts = ("%x", "%c", "%x %X", "%Y",
             "%m/%d/%Y", "%m/%d/%y", "%Y-%m-%d", "%Y/%m/%d",
             "%b %d, %Y", "%b %d %Y", "%d %b, %Y", "%d %b %Y",
             "%b. %d, %Y", "%b. %d %Y", "%d %b., %Y", "%d %b. %Y",
             "%b %Y", "%b, %Y", "%b. %Y", "%b., %Y", "%b-%Y", "%b.-%Y",
             "%B %d, %Y", "%B %d %Y", "%d %B, %Y", "%d %B %Y",
             "%B %Y", "%B, %Y", "%B-%Y")

# Common date and time formats used for parsing.
datetime_fmts = ("%x", "%c", "%x %X", "%Y",
                 "%m/%d/%Y", "%m/%d/%Y %H%M", "%m/%d/%Y %I:%M %p",
                 "%m/%d/%y", "%m/%d/%y %H%M", "%m/%d/%y %I:%M %p",
                 "%Y-%m-%d", "%Y-%m-%d %H%M", "%Y-%m-%d %I:%M %p",
                 "%Y/%m/%d", "%Y/%m/%d %H%M", "%Y/%m/%d %I:%M %p",
                 "%b %d, %Y", "%b %d, %Y %X", "%b %d, %Y %I:%M %p",
                 "%b %d %Y", "%b %d %Y %X", "%b %d %Y %I:%M %p",
                 "%d %b, %Y", "%d %b, %Y %X", "%d %b, %Y %I:%M %p",
                 "%d %b %Y", "%d %b %Y %X", "%d %b %Y %I:%M %p",
                 "%b. %d, %Y", "%b. %d, %Y %X", "%b. %d, %Y %I:%M %p",
                 "%b. %d %Y", "%b. %d %Y %X", "%b. %d %Y %I:%M %p",
                 "%d %b., %Y", "%d %b., %Y %X", "%d %b., %Y %I:%M %p",
                 "%d %b. %Y", "%d %b. %Y %X", "%d %b. %Y %I:%M %p",
                 "%b %Y", "%b, %Y", "%b. %Y", "%b., %Y", "%b-%Y", "%b.-%Y",
                 "%B %d, %Y", "%B %d, %Y %X", "%B %d, %Y %I:%M %p",
                 "%B %d %Y", "%B %d %Y %X", "%B %d %Y %I:%M %p",
                 "%d %B, %Y", "%d %B, %Y %X", "%d %B, %Y %I:%M %p",
                 "%d %B %Y", "%d %B %Y %X", "%d %B %Y %I:%M %p",
                 "%B %Y", "%B, %Y", "%B-%Y")

# Checks the validity of integer values.
# data: table cell entry
# minval: lowest possible value
# maxval: highest possible value
# required: cell must not be empty
# return: status, error message
def check_int(data, minval=-inf, maxval=inf, required=False):
    
    if not data:
        if not required:
            return 2, "" # EMPTY/UNDEFINED
        else:
            return 0, "missing entry" # ERROR
    
    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry" # WARNING

    else:
        try:
            value = int(data)
            if value < minval:
                return 1, "value too small" # WARNING
            elif value > maxval:
                return 1, "value too large" # WARNING
            else:
                return 3, "" # SUCCESS
        except ValueError:
            return 0, "not an integer" # ERROR

# Checks the validity of float values.
# data: table cell entry
# minval: lowest possible value
# maxval: highest possible value
# required: cell must not be empty
# return: status, error message
def check_float(data, minval=-inf, maxval=inf, required=False):
    
    if not data:
        if not required:
            return 2, "" # EMPTY/UNDEFINED
        else:
            return 0, "missing entry" # ERROR
    
    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry", # WARNING
    
    else:
        try:
            value = float(data)
            if value < minval:
                return 1, "value too small" # WARNING
            elif value > maxval:
                return 1, "value too large" # WARNING
            else:
                return 3, "" # SUCCESS
        except ValueError:
            return 0, "not a float" # ERROR

# Checks the validity of boolean values.
# data: table cell entry
# required: cell must not be empty
# return: status, error message
def check_bool(data, required=False):
    
    if not data:
        if not required:
            return 2, "" # EMPTY/UNDEFINED
        else:
            return 0, "missing entry" # ERROR
    
    elif data in ("NA", "NULL", "null", None):
        return 1, "undefined entry", # WARNING
    
    elif data in ('True', 'true', 'TRUE', 'Yes', 'yes', 'YES', True):
        return 3, "" # SUCCESS
    elif data in ('False', 'false', 'FALSE', 'No', 'no', 'NO', False):
        return 3, "" # SUCCESS
    else:
        return 0, "not a boolean" # ERROR

# Checks the validity of string values.
# data: table cell entry
# minlen: minimum string length
# maxlen: maximum string length
# pattern: regular expression
# required: cell must not be empty
# return: status, error message
def check_string(data, minlen=0, maxlen=inf, pattern=None, required=False):
    
    if not data:
        if not required:
            return 2, "" # EMPTY/UNDEFINED
        else:
            return 0, "missing entry" # ERROR
    
    elif len(data) < minlen:
        return 1, "data too short" # WARNING
    elif len(data) > maxlen:
        return 1, "data too long" # WARNING
    
    elif pattern:
        if pattern.match(data):
            return 3, "" # SUCCESS
        else:
            return 1, "pattern mismatch" # WARNING
    else:
        return 3, "" # SUCCESS

# Validates the table format specifications.
# col: table column name
# format: format specifications
# return: True if there are no errors/warnings
def validate_header(col, format):
    global LOG, HTM, FMT
    log = ""
    chk = 3
    
    type = "string"
    if "type" in format:
        if format["type"] in ("integer", "float", "bool", "string"):
            type = format["type"]
        else:
            log += '\n [WRN] in header, column "' + col + '": unknown type "' + format["type"] + '"'
            chk = min(chk, 1)
    
    required = False
    if "required" in format:
        if format["required"] in ("True", "true", "TRUE", "Yes", "yes", "YES"):
            required = True
        elif format["required"] not in ("False", "false", "FALSE", "No", "no", "NO"):
            log += '\n [ERR] in header, column "' + col + '": invalid value for required'
            chk = min(chk, 0)
    
    minval = -inf
    if "minval" in format:
        if type in ("integer", "float"):
            try:
                minval = float(format["minval"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for minval'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": minval for non-numeric type'
            chk = min(chk, 1)
    
    maxval = inf
    if "maxval" in format:
        if type in ("integer", "float"):
            try:
                maxval = float(format["maxval"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for maxval'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": maxval for non-numeric type'
            chk = min(chk, 1)
    
    minlen = 0
    if "minlen" in format:
        if type in ("string"):
            try:
                minlen = int(format["minlen"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for minlen'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": minlen for non string-type'
            chk = min(chk, 1)
    
    maxlen = inf
    if "maxlen" in format:
        if type in ("string"):
            try:
                maxlen = int(format["maxlen"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid value for maxlen'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": maxlen for non string-type'
            chk = min(chk, 1)
    
    pattern = None
    if "pattern" in format:
        if type in ("string"):
            try:
                pattern = re.compile(format["pattern"])
            except:
                log += '\n [ERR] in header, column "' + col + '": invalid regular expression'
                chk = min(chk, 0)
        else:
            log += '\n [WRN] in header, column "' + col + '": pattern for non string-type'
            chk = min(chk, 1)
    
    for key in format.keys():
        if key not in ("type", "required", "minval", "maxval", "minlen", "maxlen", "pattern"):
            log += '\n [WRN] in header, column "' + col + '": unknown argument "' + key + '"'
            chk = min(chk, 1)
    
    FMT[col] = {"type": type, "required": required, "minval": minval, "maxval": maxval,
                 "minlen": minlen, "maxlen": maxlen, "pattern": pattern}
    
    if chk == 0: # ERROR / RED
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#800000;"><code>' + col + '</code></span></th>\n'
    elif chk == 1: # WARNING / YELLOW
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#705000;"><code>' + col + '</code></span></th>\n'
    elif chk == 2: # UNDEFINED / WHITE
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#202020"><code>' + col + '</code></span></th>\n'
    elif chk == 3: # SUCCESS / GREEN
        HTM += '\t\t\t<th bgcolor=#EEEEEE><span style="font-size:15px;color:#004000;"><code>' + col + '</code></span></th>\n'
    
    LOG += log
    return log == ""

# Validates the table format specifications.
# data: table cell entry
# row: table row number
# col: table column name
# return: True if there are no errors/warnings
def validate_cell(data, row, col):
    global LOG, HTM, FMT
    log = ""
    
    if FMT[col]["type"] == "integer":
        chk, desc = check_int(data, FMT[col]["minval"], FMT[col]["maxval"], FMT[col]["required"])
    elif FMT[col]["type"] == "float":
        chk, desc = check_float(data, FMT[col]["minval"], FMT[col]["maxval"], FMT[col]["required"])
    elif FMT[col]["type"] == "bool":
        chk, desc = check_bool(data, FMT[col]["required"])
    elif FMT[col]["type"] == "string":
        chk, desc = check_string(data, FMT[col]["minlen"], FMT[col]["maxlen"], FMT[col]["pattern"], FMT[col]["required"])
    
    if chk == 0:
        log += '\n [ERR] in row ' + str(row) + ', column "' + col + '": ' + desc
    elif chk == 1:
        log += '\n [WRN] in row ' + str(row) + ', column "' + col + '": ' + desc
    
    if chk == 0: # ERROR / RED
        HTM += '\t\t\t<td title="' + desc + '" bgcolor=#FFBFBF><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 1: # WARNING / YELLOW
        HTM += '\t\t\t<td title="' + desc + '" bgcolor=#FFFF7F><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 2: # UNDEFINED / WHITE
        HTM += '\t\t\t<td bgcolor=#F7F7F7><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    elif chk == 3: # SUCCESS / GREEN
        HTM += '\t\t\t<td bgcolor=#BFFFBF><span style="font-size:14px;"><code>' + data + '</code></span></td>\n'
    
    LOG += log
    return log == ""

# Opens and validates the csv table.
# csv_path: path to the csv file
# fmt_path: path to the format file
# return: status, feedback
def validate_table(csv_path, fmt_path=""):
    global LOG, HTM, FMT
    LOG = ""
    HTM = ""
    FMT = {}
    
    config = {}
    status = 2
    feedback = "SUCCESS: The CSV file is well-formed. It matches the format declaration and therefore is valid."
    if not fmt_path:
        status = 1
        feedback = "WARNING: The CSV file is well-formed. Please provide a format file (.ini) to check for validity."
    
    else:
        try:
            parser = configparser.ConfigParser()
            parser.read_file(open(fmt_path, 'r'))
            for column, _ in parser.items():
                config[column] = {}
                for key, value in parser.items(column):
                    config[column][key] = value
        
        except IOError:
            status = 1
            feedback = "WARNING: The CSV file is well-formed. The format file could not be opened or is missing."
        except:
            status = 1
            feedback = "WARNING: The CSV file is well-formed. The format file could not be parsed or has errors."
    
    try:
        dialect = csv.Sniffer().sniff(open(csv_path, 'r').read(1024)) # get dialect for different delimiters
        table = csv.reader(open(csv_path, 'r'), dialect=dialect)
        
        line_nums = False
        start = 1
        for row in table:
            break
        for row in table:
            try:
                curr = int(row[0])
                if curr not in (0, 1, 2):
                    prev = -inf
                    break
                start = curr
                prev = curr
                break
            except:
                break
        for row in table:
            try:
                curr = int(row[0])
                if curr != prev + 1:
                    break
                prev = curr
            except:
                break
        else:
            line_nums = True
        
        table = csv.reader(open(csv_path, 'r'), dialect=dialect)
        
        HTM += '<html><meta charset="UTF-8">\n'
        HTM += '\t<table border="0" class="dataframe">\n'
        HTM += '\t<thead>\n'
        
        for row in table:
            header = row
            line = start - 1
            HTM += '\t\t<tr style="text-align:center;vertical-align:middle;">\n'
            if line < 1:
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#F7F7F7><span style="font-size:15px;"><code></code></span></th>\n'
            else:
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;"><code>' + str(line) + '</code></span></th>\n'
            for i in range(len(row)):
                if line_nums and i == 0:
                    continue
                if not row[i] in config:
                    config[row[i]] = {}
                    if status > 1:
                        status = 1
                        feedback = "WARNING: The CSV file is well-formed. The format declaration for some columns is missing."       
                if not validate_header(row[i], config[row[i]]) and status > 1:
                    status = 1
                    feedback = "WARNING: The CSV file is well-formed. The format declaration has some errors or warnings."
            HTM += '\t\t</tr>\n'
            break
        
        HTM += '\t</thead>\n' 
        HTM += '\t<tbody>\n'
        
        for row in table:
            line += 1
            HTM += '\t\t<tr style="text-align:center;vertical-align:middle;">\n'
            HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;"><code>' + str(line) + '</code></span></th>\n'
            if len(row) != len(header):
                status = 0
                feedback = "ERROR: The CSV file is not well-formed. The rows differ in length or columns are missing."
                break
            for i in range(len(row)):
                if line_nums and i == 0:
                    continue
                if not validate_cell(row[i], line, header[i]) and status > 1:
                    status = 1
                    feedback = "WARNING: The CSV file does not match the format declaration. It is well-formed, but not valid."
            HTM += '\t\t</tr>\n'
        
        HTM += '\t</tbody>\n'
        HTM += '\t</table>\n'
        HTM += '</html>'
    
    except IOError:
        status = 0
        feedback = "ERROR: The CSV file could not be opened. Please check if the file is missing or corrupted."
    except:
        status = 0
        feedback = "ERROR: The CSV file could not be parsed. It is not well-formed and contains syntax errors."
    
    return status, feedback

# Creates dictionary of feedback files and badge status.
# paths_dict: path of file which is checked
# path: root path of the repository
# log_path: path where feedback is stored on the server
# return: dictionary of feedback files and badge status
def create_dict_values(paths_dict, path, log_path):
    global LOG, HTM, FMT
    files_dict = {}
    for csv, fmt in paths_dict.items():
        feedback = ""
        if not fmt:
            files_dict[csv], feedback = validate_table(csv)
        else:
            files_dict[csv], feedback = validate_table(csv, fmt[0])
        val = csv.rsplit(path, 1)[1].strip("/")
        output_path = os.path.join(log_path, val)
        os.makedirs(output_path.rsplit("/", 1)[0], exist_ok = True)
        create_log(output_path + "_.log", feedback + LOG)
        create_log(output_path + "_.html", HTM)
    return files_dict

# Writes the results to a file.
# logfile: path of the logfile on the server
# content: feedback which is written into file
def create_log(logfile, content):
    with open(logfile, 'w+') as log_file:
        log_file.write(str(content))
