#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is the quality check for fair metrics.
#

import os

# Creates dictionary of feedback files and badge status.
# paths_dict: path of file which is checked
# path: root path of the repository
# log_path: path where feedback is stored on the server
# return: dictionary of feedback files and badge status
def create_dict_values(paths_dict, path, log_path):
    files_dict = {}
    for val, key in paths_dict.items():
        feedback = ""
        if key:
            files_dict[key[0]] = 2
            feedback = "SUCCESS: The {} file exists. Please make sure that it contains the necessary information.".format(val)
            val = str(key[0]).rsplit("/")[-1] # get filename from path
        else:
            tmp_key = os.path.join(path, val)
            files_dict[tmp_key] = 0
            feedback = "ERROR: The {} file does not exist. Please create and place it in the project root directory.".format(val)
        output_path = os.path.join(log_path, val) + "_.log"
        create_log(output_path, feedback)
    return files_dict

# Writes the results to a file.
# logfile: path of the logfile on the server
# content: feedback which is written into file
def create_log(logfile, content):
    with open(logfile, 'w+') as log_file:
        log_file.write(str(content))
