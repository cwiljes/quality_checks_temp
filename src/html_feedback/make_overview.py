#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is used to create feedback logs.
#

import os

ROOT = ""
PATH = ""
CONTENT = ""
STATUS = 0

# Variables containing the SVGs for the status badge.
POS = "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='17px' height='17px' version='1.1'><defs/><g transform='translate(0.5,0.5)'><ellipse cx='8' cy='8' rx='8' ry='8' fill='#009900' stroke='#009900' pointer-events='none'/><path d='M 5.25 2.5 L 10.75 2.5 L 10.75 4.5 L 7.25 4.5 L 7.25 12 L 5.25 12 Z' fill='#ffffff' stroke='#ffffff' stroke-miterlimit='10' transform='rotate(225,8,7.25)' pointer-events='none'/></g></svg>"
WRN = "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='17px' height='17px' version='1.1'><defs/><g transform='translate(0.5,0.5)'><ellipse cx='8' cy='8' rx='8' ry='8' fill='#ffcc00' stroke='#ffcc00' pointer-events='none'/><rect x='7' y='2.5' width='2' height='7.5' fill='#ffffff' stroke='#ffffff' pointer-events='none'/><ellipse cx='8' cy='13' rx='1.25' ry='1.25' fill='#ffffff' stroke='#ffffff' pointer-events='none'/></g></svg>"
ERR = "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='17px' height='17px' version='1.1'><defs/><g transform='translate(0.5,0.5)'><ellipse cx='8' cy='8' rx='8' ry='8' fill='#ff3333' stroke='#ff0000' pointer-events='none'/><path d='M 3 7 L 7 7 L 7 3 L 9 3 L 9 7 L 13 7 L 13 9 L 9 9 L 9 13 L 7 13 L 7 9 L 3 9 Z' fill='#ffffff' stroke='#ffffff' stroke-miterlimit='10' transform='rotate(45,8,8)' pointer-events='none'/></g></svg>"

# Starts a new HTML document.
# root: repository path
# path: output path
def init(root, path):
    global ROOT, PATH, CONTENT, STATUS
    ROOT = root
    PATH = path
    CONTENT = '<html><meta charset="UTF-8">\n'
    STATUS = 2

# Adds a new section to the document.
# name: quality check section name
# repo_name: name of the repository
# files: dictionary with files and quality values
# display_log: file should have a log entry
# display_htm: file should have a html entry
def add_section(name, repo_name, files, display_log=True, display_htm=False):
    global ROOT, PATH, CONTENT, STATUS
    CONTENT += '\t<h2><span style="font-size:24px;"><code>' + name + '</code></span></h2>\n'
    CONTENT += '\t<table border="0" class="dataframe">\n'
    CONTENT += '\t<tbody>\n'
    for (key, value) in sorted(files.items()):
        key = os.path.join(key.rsplit("/", 1)[0], key.rsplit("/", 1)[1])
        log = key.rsplit(ROOT, 1)[1].strip("/") + "_.log"
        htm = key.rsplit(ROOT, 1)[1].strip("/") + "_.html"
        key = key.rsplit(repo_name.split("/")[0])[-1]
        
        CONTENT += '\t\t<tr style="vertical-align:middle;">\n'
        if value == 0:
            CONTENT += '\t\t\t<td><span>' + ERR + '</span></td>'
            CONTENT += '\t\t\t<td><span style="font-size:16px;color:red;"><code>' + key + '</code></span></td>'
        elif value == 1:
            CONTENT += '\t\t\t<td><span>' + WRN + '</span></td>'
            CONTENT += '\t\t\t<td><span style="font-size:16px;color:orange;"><code>' + key + '</code></span></td>'
        elif value == 2:
            CONTENT += '\t\t\t<td><span>' + POS + '</span></td>'
            CONTENT += '\t\t\t<td><span style="font-size:16px;color:green;"><code>' + key + '</code></span></td>'
        if display_log:
            CONTENT += '\t\t\t<td><span style="font-size:15px;color:gray;"><code><a href="' + log + '">LOG</a></code></span></td>'
        if display_htm:
            CONTENT += '\t\t\t<td><span style="font-size:15px;color:gray;"><code><a href="' + htm + '">HTML</a><code></span></td>'
        CONTENT += '\t\t</tr>\n'
        
        STATUS = min(value, STATUS)
    CONTENT += '\t</tbody>\n'
    CONTENT += '\t</table>\n'

# Writes the document to the output path.
# path: output path
# return: overall badge status
def finalize(path):
    global ROOT, PATH, CONTENT, STATUS
    CONTENT += '</html>'
    result_path = os.path.join(PATH, "result.html")
    website_path = os.path.join(path, "result.html")
    file = open(result_path, 'w')
    file.write(CONTENT)
    file.close()
    create_json(STATUS, website_path, os.path.join(PATH, "badge.json"))
    return STATUS

# Creates JSON badge with global test results for use in PUB. 
# status: overall badge status
# result_path: feedback url
# path: output path
def create_json(status, result_path, path):
    comment = ""
    svg = ""
    if status == 0:
        label = "not well-formed"
        icon = ERR
    elif status == 1:
        label = "well-formed"
        icon = WRN
    elif status == 2:
        label = "valid"
        icon = POS
    content = '{\n\t"label": "' + label + '", \n\t"status": ' + str(status) + ', \n\t"icon": "' + icon + '", \n\t"url": "' + result_path + '" \n}'
    with open(path, 'w') as json_file:
        json_file.write(str(content))
