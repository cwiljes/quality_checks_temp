#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Main script to execute quality check pipeline.
# It calls all the checks, creates the feedback and sends an email to the user.
#

import os, argparse
import fair_check.fair_check as fc
import csv_check.csv_check as cc
import xml_check.xml_check as xc
import html_feedback.make_overview as mo
import mail.send_mail as sm
from subprocess import Popen, PIPE

# Argument parser is used to get all needed paths and user specific CI variables.
parser = argparse.ArgumentParser(description="Process csv checking and feedback")
parser.add_argument("--feedback", "-f", dest="feedback", type=str, required=True, help="Set the path on the server where the feedback files are stored.")
parser.add_argument("--link", "-l", dest="link", type=str, required=True, help="Set the webpage where the feedback_folder is shown.")
parser.add_argument("--root_path", "-r", dest="root_path", type=str, required=True, help="Set the path to the repository.")
parser.add_argument("--data", "-d", dest="data", type=str, required=False, help="Set the path to the data inside the repository.")
parser.add_argument("--gitlab_user_name", "-gn", dest="gitlab_user_name", type=str, required=True, help="Set the user name.")
parser.add_argument("--gitlab_user_email", "-ge", dest="gitlab_user_email", type=str, required=True, help="Set the user email.")
parser.add_argument("--gitlab_project_url", "-gu", dest="gitlab_project_url", type=str, required=True, help="Set the url to the gitlab project.")
parser.add_argument("--gitlab_project_path", "-gp", dest="gitlab_project_path", type=str, required=True, help="Set gitlab path for the project.")
parser.add_argument("--gitlab_commit_sha", "-gs", dest="gitlab_commit_sha", type=str, required=True, help="Set the commit id.")
args = parser.parse_args()

# Create paths from argparser as string variables.
s_path = args.feedback
w_path = args.link
r_path = args.root_path
d = args.data
d_path = os.path.join(r_path, d)
gitlab_user_name = args.gitlab_user_name
gitlab_user_email = args.gitlab_user_email
gitlab_project_url = args.gitlab_project_url
gitlab_project_path = args.gitlab_project_path
gitlab_commit_sha = args.gitlab_commit_sha
server_path = os.path.join(s_path, gitlab_project_path, gitlab_commit_sha)
website_path = os.path.join(w_path, gitlab_project_path, gitlab_commit_sha)

# Main function executes everything.
def main():
    # Create feedback path on the server where feedback is stored.
    os.makedirs(server_path, exist_ok = True)
    
    # Search and select files which should be checked.
    fair_files = ["AUTHORS", "LICENSE", "README"]
    fair_dict = {}
    for path in fair_files:
        # Check for plain file name "AUTHORS" without extension or "AUTHORS.txt/md" but only accept one option.
        if find_file(r_path, path + ".md"):
            fair_dict[path] = find_file(r_path, path + ".md")
        elif find_file(r_path, path + ".txt"):
            fair_dict[path] = find_file(r_path, path + ".txt") 
        else:
            fair_dict[path] = find_file(r_path, path)
   
    csv_files = find_file(d_path, "*.csv")
    csv_dict = {}
    for path in csv_files:
        p = path.rsplit("/", 1)
        csv_dict[path] = find_file(p[0], p[1].rsplit(".", 1)[0] + ".ini")
    
    xml_files = find_file(d_path, "*.xml")
    xml_dict = {}
    for path in xml_files:
        p = path.rsplit("/", 1)
        xml_dict[path] = find_file(p[0], p[1].rsplit(".", 1)[0] + ".dtd")
    
    # Get file paths and feedback if file was found.
    fair_result_dict = fc.create_dict_values(fair_dict, r_path, server_path)
    csv_result_dict = cc.create_dict_values(csv_dict, r_path, server_path)
    xml_result_dict = xc.create_dict_values(xml_dict, r_path, server_path)
    
    # Create html and log files from feedback.
    mo.init(r_path, server_path)
    mo.add_section("FAIR metrics", gitlab_project_path, fair_result_dict)
    mo.add_section("CSV checks", gitlab_project_path, csv_result_dict, display_htm=True)
    mo.add_section("XML checks", gitlab_project_path, xml_result_dict, display_htm=True)
    badge_status = mo.finalize(website_path)

    # Create mail content and send email to the user.
    mail_content = sm.create_content(gitlab_user_name, gitlab_project_url, gitlab_commit_sha, website_path, badge_status)
    sm.send_mail(gitlab_user_email, gitlab_project_url, mail_content)

# Use bash command to search a path for a file.
# path: directory which is searched
# file_name: file name which is searched
# return: return path for a searched file.
def find_file(path, file_name):
    p = Popen(["find", path, "-type", "f", "-iname", file_name], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, _ = p.communicate()
    file_path = str(output).strip("b'").split("\\n")
    file_path.remove("")
    return file_path

if __name__ == "__main__":
    main()
