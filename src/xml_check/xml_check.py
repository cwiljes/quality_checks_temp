#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is the quality check for XML files.
#

import os
from lxml import etree

# Global variables for feedback.
LOG = ""
HTM = ""

# Valiadates the XML (and optional DTD) file.
# xml_path: path of the XML file
# dtd_path: optional path of the DTD file
# return: badge status as int and feedback string
def validate_xml(xml_path, dtd_path=""):
    global LOG, HTM
    LOG = ""
    HTM = ""
    
    errors = {}
    warnings = {}

    etree.clear_error_log() # clear error_log for each file
    
    # Check if DTD exists and if XML is well-formed against given DTD file.
    if dtd_path:
        status = 2
        feedback = "SUCCESS: The XML file matches the doctype definition and therefore is valid."
        parser = etree.XMLParser(dtd_validation=True)
    else:
        status = 1
        feedback = "WARNING: The XML file is well-formed. Please provide a doctype file (.dtd) to check for validity."
        parser = etree.XMLParser(dtd_validation=False)
    
    # Parse XML file.
    try:
        tree = etree.parse(xml_path)
    # Check XML for IO errors.
    except IOError:
        status = 0
        feedback = "ERROR: The XML file could not be opened. Please check if the file is missing or corrupted."
    # Check XML for syntax errors.
    except etree.XMLSyntaxError as err:
        status = 0
        feedback = "ERROR: The XML file could not be parsed. It is not well-formed and contains syntax errors."
        for e in err.error_log:
            msg = str(e).split(": ", 1)[-1]
            line_number = int(str(e).split(":")[1])
            errors[line_number] = msg
            LOG += "\n [ERR] in line " + str(line_number) + ": " + msg

    # Parse DTD file.
    if dtd_path:
        try:
            dtd = etree.DTD(open(dtd_path))
            if not dtd.validate(tree):
                if status >= 1:
                    status = 1
                    feedback = "WARNING: The XML file does not match the doctype definition. It is well-formed, but not valid."
                for e in dtd.error_log:
                    msg = str(e).split(": ", 1)[-1]
                    line_number = int(str(e).split(":")[1])
                    warnings[line_number] = msg
                    LOG += "\n [WRN] in line " + str(line_number) + ": " + msg    
        # Check DTD for IO errors.
        except IOError:
            if status >= 1:
                status = 1
                feedback = "WARNING: The XML file is well-formed. The doctype file could not be opened or is missing."
        # Check DTD for syntax errors.
        except:
            if status >= 1:
                status = 1
                feedback = "WARNING: The XML file is well-formed. The doctype file could not be parsed or has errors."
    
    # Set header for HTML feedback.
    HTM += '<html><meta charset="UTF-8">\n'
    HTM += '\t<table border="0" class="dataframe">\n'
    HTM += '\t<tbody>\n'
    
    # Display XML file in HTML and mark the errors and warnings.
    try:
        file = open(xml_path, 'r')
        line_number = 0
        for line in file:
            line_number += 1
            line = line.replace('"', '&#34;').replace('<', '&#60;').replace('>', '&#62;')
            HTM += '\t\t<tr style="vertical-align:middle;">\n'
            if line_number in errors.keys():
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;color:#A00000;"><code>' + str(line_number) + '</code></span></th>\n'
                HTM += '\t\t\t<td title="' + errors[line_number] + '" style="text-align:left;" bgcolor=#FFFFFF><span style="font-size:14px;color:#C00000;"><pre>' + line + '</pre></span></td>\n'
            elif line_number in warnings.keys():
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;color:#8C6400;"><code>' + str(line_number) + '</code></span></th>\n'
                HTM += '\t\t\t<td title="' + warnings[line_number] + '" style="text-align:left;" bgcolor=#FFFFFF><span style="font-size:14px;color:#A87800;"><pre>' + line + '</pre></span></td>\n'
            else:
                HTM += '\t\t\t<th style="text-align:right;" bgcolor=#EEEEEE><span style="font-size:15px;color:#005000;"><code>' + str(line_number) + '</code></span></th>\n'
                HTM += '\t\t\t<td style="text-align:left;" bgcolor=#FFFFFF><span style="font-size:14px;color:#006000;"><pre>' + line + '</pre></span></td>\n'
            HTM += '\t\t</tr>\n'
        file.close()
    except:
        pass
    
    # Set footer for HTML feedback.
    HTM += '\t</tbody>\n'
    HTM += '\t</table>\n'
    HTM += '</html>'
    
    return status, feedback

# Creates dictionary of feedback files and badge status.
# paths_dict: path of file which is checked
# path: root path of the repository
# log_path: path where feedback is stored on the server
# return: dictionary of feedback files and badge status
def create_dict_values(paths_dict, path, log_path):
    global LOG, HTM
    LOG = ""
    HTM = ""
    files_dict = {}
    for xml, dtd in paths_dict.items():
        feedback = ""
        if not dtd:
            files_dict[xml], feedback = validate_xml(xml)
        else:
            files_dict[xml], feedback = validate_xml(xml, dtd[0])   
        val = xml.rsplit(path, 1)[1].strip("/")
        output_path = os.path.join(log_path, val)
        os.makedirs(output_path.rsplit("/", 1)[0], exist_ok = True)
        create_log(output_path + "_.log", feedback + LOG)
        create_log(output_path + "_.html", HTM)
    return files_dict

# Writes the results to a file.
# logfile: path of the logfile on the server
# content: feedback which is written into file
def create_log(logfile, content):
    with open(logfile, 'w+') as log_file:
        log_file.write(str(content))
